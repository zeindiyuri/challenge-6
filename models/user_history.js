'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.user_history.belongsTo(models.user_games, {
        foreignKey: "user_id",
        as: "history"
      })
    }
  }
  user_history.init({
    user_id: DataTypes.INTEGER,
    score: DataTypes.INTEGER,
    high_score: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_history',
  });
  return user_history;
};