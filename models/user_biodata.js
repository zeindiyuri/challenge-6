'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.user_biodata.belongsTo(models.user_games, {
        foreignKey: "user_id",
        as: "biodata"
      })
    }
  }
  user_biodata.init({
    user_id: DataTypes.INTEGER,
    full_name: DataTypes.STRING,
    adress: DataTypes.STRING,
    city: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_biodata',
  });
  return user_biodata;
};