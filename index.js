const express = require ('express');
const app = express();
const fs = require ('fs');
const {user_games, user_biodata, user_history}= require ('./models')



app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(express.static("./public"));
app.set("view engine", "ejs");

app.post('/users', (req,res) =>{
    const {username, email, password}= req.body;

    user_games.create({
        username: username,
        email: email,
        password: password,
        })
        .then((User_games) => {
        res.json({message: "Berhasil", data: User_games})
        })
        .catch ((err) => {
            console.log (err);
        })
})

app.post('/biodata', (req,res) =>{
    const {user_id, full_name, adress, city}= req.body;

    user_biodata.create({
        user_id: user_id,
        full_name: full_name,
        adress : adress,
        city: city,
        })
        .then((User_biodata) => {
        res.json({message: "Berhasil", data: User_biodata})
        })
        .catch ((err) => {
            console.log (err);
        })
})

app.post('/history', (req,res) =>{
    const {user_id, score, high_score}= req.body;

    user_history.create({
        user_id: user_id,
        score : score,
        high_score : high_score,
        })
        .then((User_histories) => {
        res.json({message: "Berhasil", data: User_histories})
        })
        .catch ((err) => {
            console.log (err);
        })
})

app.put('/users/:id', (req,res)=> {
    const { id } = req.params;
    user_games.update({where: {id:id}})
    .then(() => {
        res.json({message: "update sucess"});
    })

    .catch((err) => {
        res.json({message: "gagal update"})
    })
  
});

app.delete('/users/:id', (req,res)=> {
    const {id} = req.params;
    user_games.destroy({where: { id:id } } )
    .then(() => {
        res.json({message: "delete sucess"});
    })

    .catch((err) => {
        res.json({message: "gagal delete"})
    });
});



app.get("/users", (req,res) => {
    user_games.findAll({include : ["history", "biodata"]})
    .then((Users) => {
      res.json ({message: "berhasil", data:Users})
    })
})


// app.get ('/users', (req,res) => {
//     user_games.findAll().then((User_games) => {
//         res.json({message: "fetch sucess", data: User_games})
//     })
// })

// app.get ('/biodata', (req,res) => {
//     user_biodata.findAll().then((User_biodata) => {
//         res.json({message: "fetch sucess", data: User_biodata})
//     })
// })

// app.get ('/users/:id', (req,res) => {
//     const { id } = req.params;

//     user_games.findOne({where: {id:id}}).then((User_games) => {
//         res.json({message: "fetch sucess", data: User_games})
//     })
// })

// app.get ('/history', (req,res) => {
//     user_history.findAll().then((User_history) => {
//         res.json({message: "fetch sucess", data: User_history})
//     })
// })

app.listen (3000, () => console.log (`listening at http://localhost:${3000}`));
